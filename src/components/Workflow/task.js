import React from "react";
import { Form, Card, Button, InputGroup } from "react-bootstrap";
import { FaCheck } from "react-icons/fa";

function Task(props) {
  const [taskForm, setTaskForm] = React.useState(props.task);

  function handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    setTaskForm({
      ...taskForm,
      [name]: value,
    });
    props.parentCallback(taskForm, props.task.id);
  }

  function taskStatusUpdate(e) {
    e.preventDefault();
    let task = { ...taskForm };
    if (task.status === "Pending") {
      task.status = "In Progress";
    } else if (task.status === "In Progress") {
      task.status = "Completed";
    } else {
      task.status = "Pending";
    }
    setTaskForm({
      ...taskForm,
      ...task,
    });
    props.parentCallback(task, props.task.id);
  }

  return (
    <div className="task-list-container">
      <div className="task-list-status-icon">
        <Button
          className={
            taskForm.status === "Pending"
              ? "btn btn-circle btn-secondary"
              : taskForm.status === "In Progress"
              ? "btn btn-circle btn-primary"
              : "btn btn-circle btn-success"
          }
          onClick={taskStatusUpdate}
        >
          <FaCheck />
        </Button>
      </div>
      <Card className="task-list">
        <Card.Body className="mt-2">
          <Form>
            <Form.Group>
              <Form.Control
                type="text"
                name="title"
                placeholder="Title"
                value={taskForm.title}
                onChange={handleInputChange}
                required
              />
              <Form.Control.Feedback type="invalid">
                Please enter a valid email.
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <InputGroup>
                <Form.Control
                  as="textarea"
                  type="textarea"
                  name="content"
                  rows={10}
                  placeholder="Content"
                  value={taskForm.content}
                  onChange={handleInputChange}
                  required
                />
                <Form.Control.Feedback type="invalid">
                  Please enter a valid email.
                </Form.Control.Feedback>
              </InputGroup>
            </Form.Group>
          </Form>
        </Card.Body>
      </Card>
    </div>
  );
}

export default Task;
