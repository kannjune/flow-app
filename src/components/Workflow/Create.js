import React, { useEffect } from "react";
// form design
import { Container, Row, Form, Button, Col } from "react-bootstrap";
import { FaPlus } from "react-icons/fa";
import { AiOutlineClose } from "react-icons/ai";
import { ImShuffle } from "react-icons/im";

import Task from "./task";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import * as workflowActions from "../../redux/actions/workflowActions";

import { toast } from "react-toastify";

function Create(props) {
  const [showShuffle, setShowShuffle] = React.useState(false);
  const [workflowForm, setWorkflowForm] = React.useState({
    id: (Math.floor(Math.random() * 9000) + 1000).toString(),
    workflowName: "",
    workflowStatus: "Pending",
    taskList: [
      {
        id: (Math.floor(Math.random() * 900) + 100).toString(),
        title: "",
        content: "",
        status: "Pending",
      },
    ],
  });
  useEffect(() => {
    if (props.match.params["id"]) {
      props.loadWorkflows();
      let editData = props.workflowList.find(
        (obj) => obj.id === props.match.params["id"]
      );
      setWorkflowForm({
        ...workflowForm,
        ...editData,
      });
    }
    let taskList = [...workflowForm.taskList];
    let pendingTasks = taskList.filter((item) => item.status !== "Completed");
    if (pendingTasks.length === 0) {
      setShowShuffle(true);
    }
  }, [showShuffle]);

  const callbackFunction = (childEvent, id) => {
    const workflow = workflowForm;
    workflow.taskList = workflow.taskList.map((task) =>
      task.id === id ? childEvent : task
    );
    setWorkflowForm({
      ...workflow,
      taskList: [...workflow.taskList],
    });
  };
  function shuffle() {
    let workflow = workflowForm;
    const shuffled = workflow.taskList
      .map((a) => [Math.random(), a])
      .sort((a, b) => a[0] - b[0])
      .map((a) => a[1]);
    setWorkflowForm({
      ...workflow,
      taskList: [...shuffled],
    });
  }

  function addNode() {
    let workflow = workflowForm;
    const obj = {
      id: (Math.floor(Math.random() * 900) + 100).toString(),
      title: "",
      content: "",
      status: "Pending",
    };
    setWorkflowForm({
      ...workflow,
      taskList: [...workflow.taskList, obj],
    });
  }

  function handleWorkflowInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    let workflow = workflowForm;
    setWorkflowForm({
      ...workflow,
      [name]: value,
    });
  }

  function removeNode() {
    let workflow = { ...workflowForm };
    let taskList = [...workflow.taskList];
    taskList = [...taskList.splice(-1, 1), ...taskList.splice(1)];
    setWorkflowForm({
      ...workflowForm,
      taskList: taskList,
    });
  }

  function submit(e) {
    e.preventDefault();
    let taskValues = workflowForm.taskList.map((obj) => {
      let result = Object.values(obj).some((o) => o === "");
      return result ? obj : null;
    });
    var taskEmptyData = taskValues.filter((x) => x !== null);
    if (taskEmptyData.length > 0) {
      toast.warning("Task Can't be added empty");
      return false;
    }
    if (workflowForm.workflowName === "") {
      toast.warning("Please enter the workflow name");
      return false;
    }
    if (props.match.params["id"]) {
      props.updateWorkflow(workflowForm);
    } else {
      props.saveWorkflow(workflowForm);
    }
    props.history.push("/dashboard");
  }

  return (
    <>
      <Container className="mt-4">
        <Row className="mb-4">
          <Col md={7}>
            <Row>
              <Col md={7}>
                <Form.Control
                  type="text"
                  placeholder="WORKFLOW NAME"
                  name="workflowName"
                  value={workflowForm.workflowName}
                  onChange={handleWorkflowInputChange}
                />
              </Col>
            </Row>
          </Col>
          <Col md={5} className="text-right workflow-action">
            {showShuffle ? (
              <>
                <Button
                  type="submit"
                  style={{ backgroundColor: "#7701B5", borderColor: "#7701B5" }}
                  onClick={shuffle}
                >
                  <ImShuffle className="svg" />
                  Shuffle
                </Button>
              </>
            ) : (
              <></>
            )}
            <Button variant="danger" type="submit" onClick={removeNode}>
              <AiOutlineClose className="svg" />
              Delete
            </Button>
            <Button variant="success" type="submit" onClick={addNode}>
              <FaPlus className="svg" />
              Add Node
            </Button>
            <Button variant="primary" type="submit" onClick={submit}>
              Save
            </Button>
          </Col>
        </Row>
      </Container>
      <hr />
      <Container className="task-container">
        <Row>
          {workflowForm.taskList.map((item) => (
            <Task
              key={item.id}
              task={item}
              workflow={workflowForm}
              parentCallback={callbackFunction}
            />
          ))}
        </Row>
      </Container>
    </>
  );
}

Create.propTypes = {
  saveWorkflow: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    workflowList: state.workflowList,
  };
}

const mapDispatchToProps = {
  saveWorkflow: workflowActions.saveWorkflow,
  loadWorkflows: workflowActions.loadWorkflows,
  updateWorkflow: workflowActions.updateWorkflow,
};
export default connect(mapStateToProps, mapDispatchToProps)(Create);
