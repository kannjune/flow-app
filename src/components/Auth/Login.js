import React from "react";
import { Container, Row, Form, Card, Button, Col } from "react-bootstrap";
import { AiOutlineMail } from "react-icons/ai";
import { VscStarFull } from "react-icons/vsc";
import { useHistory } from "react-router-dom";

function Login(props) {
  const [loginForm, setloginForm] = React.useState({ email: "", password: "" });
  const [validated, setValidated] = React.useState(false);

  const history = useHistory();

  function authenticateUser(event) {
    event.preventDefault();
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
      setValidated(true);
    } else {
      let user = JSON.stringify(loginForm);
      localStorage.setItem("user", user);
      history.push("/Dashboard");
    }
  }
  function handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    setloginForm({
      ...loginForm,
      [name]: value,
    });
  }
  return (
    <Container fluid>
      <Row className="justify-content-md-center mt-5">
        <Col md={4}>
          <Card className="shadow p-3 mb-5 bg-white rounded">
            <Card.Body>
              <Card.Text className="text-center mb-4 mt-2">Login</Card.Text>
              <Form
                onSubmit={authenticateUser}
                validated={validated}
                noValidate
              >
                <Form.Group controlId="formBasicEmail" className="mb-4">
                  <div className="form-group has-icon">
                    <span className="form-control-icon">
                      <AiOutlineMail icon="email" />
                    </span>
                    <Form.Control
                      type="email"
                      className="form-control"
                      placeholder="Email"
                      name="email"
                      value={loginForm.email}
                      onChange={handleInputChange}
                      required
                    />
                    <Form.Control.Feedback type="invalid">
                      Please enter a valid email.
                    </Form.Control.Feedback>
                  </div>
                </Form.Group>
                <Form.Group controlId="formBasicPassword" className="mb-4">
                  <div className="form-group has-icon">
                    <span className="form-control-icon">
                      <VscStarFull icon="passowrd" />
                    </span>
                    <Form.Control
                      type="password"
                      className="form-control"
                      placeholder="Password"
                      name="password"
                      value={loginForm.password}
                      onChange={handleInputChange}
                      required
                    />
                    <Form.Control.Feedback type="invalid">
                      Please enter a valid password.
                    </Form.Control.Feedback>
                  </div>
                  <Form.Text className="text-muted"></Form.Text>
                </Form.Group>
                <Form.Group controlId="formBasicCheckbox">
                  <Form.Check type="checkbox" label="Remember Me" />
                </Form.Group>
                <Button variant="primary" type="submit" block>
                  Login
                </Button>
              </Form>
            </Card.Body>
            <Card.Text className="text-muted text-center">
              <a href="/">Don't have account?. Sign Up</a>
            </Card.Text>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
export default Login;
