import React from "react";
import { Navbar } from "react-bootstrap";
import { ImTree } from "react-icons/im";

const AccountLayout = (props) => {
  return (
    <Navbar className="bg-light justify-content-between navbar-bg">
      <Navbar.Brand href="#home" className="font-white ">
        <ImTree />
        <h5>Flow App</h5>
      </Navbar.Brand>
    </Navbar>
  );
};

export default AccountLayout;
