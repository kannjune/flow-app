import React from "react";
import { Navbar, Button } from "react-bootstrap";
import { ImTree } from "react-icons/im";
import { useHistory } from "react-router-dom";

const HomeLayout = (props) => {
  const { children } = props;
  const history = useHistory();

  function logout() {
    localStorage.clear();
    history.push("/");
  }

  return (
    <>
      <Navbar className="bg-light justify-content-between navbar-bg">
        <Navbar.Brand href="#home" className="font-white ">
          <ImTree />
          <h5>Flow App</h5>
        </Navbar.Brand>
        <Button type="submit" variant="light" onClick={logout}>
          Logout
        </Button>
      </Navbar>
      <div>{children}</div>
    </>
  );
};

export default HomeLayout;
