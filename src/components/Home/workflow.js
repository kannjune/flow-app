import React from "react";
import { Form, Card, Button } from "react-bootstrap";
import { FaCheck, FaTrashAlt } from "react-icons/fa";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import * as workflowActions from "../../redux/actions/workflowActions";

import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";

const Workflow = (props) => {
  const history = useHistory();

  function handleChange() {
    let pendingTaskData = props.workflow.taskList.filter(
      (obj) => obj.status !== "Completed"
    );
    if (pendingTaskData.length > 0) {
      toast.warning(
        "Before completing the workflow all the task should be completed "
      );
    } else {
      let updateWorkflow = {
        ...props.workflow,
        workflowStatus:
          props.workflow.workflowStatus === "Completed"
            ? "Pending"
            : "Completed",
      };
      props.statusChange(updateWorkflow);
    }
  }

  function editWorkflow() {
    history.push("/edit/" + props.workflow.id);
  }
  function remove() {
    let workflow = props.workflow;
    props.removeWorkflow(workflow);
  }

  return (
    <div className="workflow-trash-icon">
      <div className="workflow-trash-icon-position">
        <Button className=" btn btn-danger btn-circle" onClick={remove}>
          <FaTrashAlt />
        </Button>
      </div>
      <Card className="m-3 shadow-sm  bg-white rounded">
        <Card.Body>
          <Form className="mb-4">
            <Form.Control
              type="text"
              placeholder="Workflow name"
              name="workflowName"
              value={props.workflow.workflowName}
              onChange={editWorkflow}
            />
          </Form>
          <Card.Text className="workflow-list-card-content ml-1">
            {props.workflow.workflowStatus}
            <Button
              className={
                "btn btn-circle" +
                (props.workflow.workflowStatus === "Completed"
                  ? "btn btn-circle btn-success"
                  : "btn btn-circle  btn-secondary")
              }
              onClick={handleChange}
            >
              <FaCheck />
            </Button>
          </Card.Text>
        </Card.Body>
      </Card>
    </div>
  );
};
Workflow.propTypes = {
  statusChange: PropTypes.func.isRequired,
  // workflowChangeData: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    // workflowChangeData: state.workflowChangeData,
  };
}

const mapDispatchToProps = {
  statusChange: workflowActions.statusChange,
  removeWorkflow: workflowActions.removeWorkflow,
};
export default connect(mapStateToProps, mapDispatchToProps)(Workflow);
