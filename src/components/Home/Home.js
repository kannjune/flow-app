import React, { useEffect } from "react";
import { Container, Row, Form, Button, Col, Dropdown } from "react-bootstrap";
import { FaPlus, FaSearch, FaFilter } from "react-icons/fa";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import * as workflowActions from "../../redux/actions/workflowActions";
import Workflow from "./workflow";

const Home = (props) => {
  const [filter, setFilter] = React.useState("");
  const [searchForm, setSearchForm] = React.useState("");

  var { workflowList } = props;

  function createNavigation() {
    props.history.push("/create");
  }

  useEffect(() => {
    props.loadWorkflows();
  }, [searchForm, filter]);

  const handleSelect = (e) => {
    setFilter(e);
  };

  function searchWorkflow(event) {
    let values = event.target.value;
    setSearchForm(values);
    workflowList = workflowList.filter((obj) => {
      return obj.workflowName.indexOf(searchForm.toLowerCase()) !== -1;
    });
  }

  return (
    <>
      <Container className="mt-4">
        <Row className="mb-2">
          <Col md={6}>
            <Row className="search-control">
              <Col>
                <div className="form-group has-icon">
                  <span className="form-control-icon">
                    <FaSearch icon="search" />
                  </span>
                  <Form.Control
                    type="text"
                    placeholder="Search Workflows"
                    value={searchForm}
                    onChange={searchWorkflow}
                    name="search"
                  />
                </div>
              </Col>
              <Dropdown
                alignRight
                id="dropdown-menu-align-right"
                onSelect={handleSelect}
              >
                <Dropdown.Toggle variant="primary" id="dropdown-basic">
                  <FaFilter /> Filter
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item eventKey="">All</Dropdown.Item>
                  <Dropdown.Item eventKey="Pending">Pending</Dropdown.Item>
                  <Dropdown.Item eventKey="Completed">Completed</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Row>
          </Col>
          <Col md={{ span: 4, offset: 2 }} className="text-right">
            <Button variant="success" type="submit" onClick={createNavigation}>
              <FaPlus className="svg" />
              Create Workflow
            </Button>
          </Col>
        </Row>
      </Container>
      <hr />
      <Container className="workflow-list-container">
        <Row>
          {workflowList && workflowList.length > 0
            ? workflowList
                .filter((obj) => {
                  if (searchForm) {
                    return (
                      obj.workflowName.indexOf(searchForm.toLowerCase()) !== -1
                    );
                  } else if (filter) {
                    return obj.workflowStatus === filter;
                  } else {
                    return obj;
                  }
                })
                .map((item) => <Workflow key={item.id} workflow={item} />)
            : []}
        </Row>
      </Container>
    </>
  );
};

Home.propTypes = {
  loadWorkflows: PropTypes.func.isRequired,
  workflowList: PropTypes.array.isRequired,
};

function mapStateToProps(state) {
  return {
    workflowList: state.workflowList,
  };
}

const mapDispatchToProps = {
  loadWorkflows: workflowActions.loadWorkflows,
};
export default connect(mapStateToProps, mapDispatchToProps)(Home);
