import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import AccountLayout from "./components/Layout/AccountLayout";
import HomeLayout from "./components/Layout/HomeLayout";

import Login from "./components/Auth/Login";
import Home from "./components/Home/Home";
import Create from "./components/Workflow/Create";

// external
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "./Style.css";

function App() {
  const AccountContainer = () => (
    <>
      <AccountLayout />
      <Route exact path="/" component={Login} />
    </>
  );
  const HomeContainer = () => (
    <HomeLayout>
      <Route path="/dashboard" component={Home} />
      <Route path="/create" component={Create} />
      <Route path="/edit/:id" component={Create} />
    </HomeLayout>
  );
  return (
    <>
      <BrowserRouter>
        <ToastContainer />
        <Switch>
          <Route exact path="/" component={AccountContainer} />
          <Route path="/dashboard" component={HomeContainer} />
          <Route path="/create" component={HomeContainer} />
          <Route path="/edit/:id" component={HomeContainer} />
        </Switch>
      </BrowserRouter>
    </>
  );
}

export default App;
