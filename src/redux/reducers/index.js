import { combineReducers } from "redux";
import workflowList from "./workflowReducer";

const rootReducer = combineReducers({
  workflowList,
});

export default rootReducer;
