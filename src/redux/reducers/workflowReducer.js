import * as types from "../actions/actionTypes";

const initialstate = [];

export default function workflowReducer(state = initialstate, action) {
  switch (action.type) {
    case types.LOAD_WORKFLOW_SUCCESS:
      return [...state];
    case types.SAVE_WORKFLOW:
      return [{ ...action.workflow }, ...state];
    case types.UPDATE_WORKFLOW:
      let updatedState = [...state];
      updatedState = state.map((obj) =>
        obj.id === action.workflow.id ? action.workflow : obj
      );
      return [...updatedState];

    case types.WORKFLOW_STATE_UPDATE:
      return state.map((obj) =>
        obj.id === action.workflow.id ? action.workflow : obj
      );
    case types.REMOVE_WORKFLOW:
      return state.filter((item) => item.id !== action.workflow.id);

    default:
      return state;
  }
}
