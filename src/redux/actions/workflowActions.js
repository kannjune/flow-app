import * as types from "./actionTypes";

const workflowList = [
  {
    id: Math.random(6).toString(),
    workflowName: "",
    workflowStatus: "",
    taskList: [
      {
        id: Math.random(6).toString(),
        title: "",
        content: "",
        status: "pending",
      },
    ],
  },
];

export function loadWorkflowSuccess(workflowList) {
  return { type: types.LOAD_WORKFLOW_SUCCESS, workflowList };
}
export function loadWorkflows() {
  return function (dispatch) {
    return dispatch(loadWorkflowSuccess(workflowList));
  };
}

export function createWorkflowSuccess(workflow) {
  return { type: types.SAVE_WORKFLOW, workflow };
}
export function saveWorkflow(workflow) {
  return function (dispatch) {
    return dispatch(createWorkflowSuccess(workflow));
  };
}

export function statusChangeSuccess(workflow) {
  return { type: types.WORKFLOW_STATE_UPDATE, workflow };
}
export function statusChange(workflow) {
  return function (dispatch) {
    return dispatch(statusChangeSuccess(workflow));
  };
}

export function updateWorkflowSuccess(workflow) {
  return { type: types.UPDATE_WORKFLOW, workflow };
}
export function updateWorkflow(workflow) {
  return function (dispatch) {
    return dispatch(updateWorkflowSuccess(workflow));
  };
}

export function removeWorkflowSuccess(workflow) {
  return { type: types.REMOVE_WORKFLOW, workflow };
}
export function removeWorkflow(workflow) {
  return function (dispatch) {
    return dispatch(removeWorkflowSuccess(workflow));
  };
}
